# mobian-i3wm

![screenshot](./doc/screenshot_01.png)

![screenshot](./doc/screenshot_02.png)

Mobian i3wm is a [mobian](https://mobian-project.org/) version with i3wm as a desktop environment for the **pinephone**.
This repo is a shameless copy of [mobian-recipes](https://gitlab.com/mobian1/mobian-recipes) but stripped to a minimum.
On top of this minimal install the following packages are installed and configured.

* lightdm
* i3-wm
* tmux (for ssh purposes)
* zsh and ohmyzsh
* rofi
* onboard (with autocorrect and word prediction)
* network-manager-gnome (for all connections and nm-applet)
* urxvt
* scrot
* polybar
* mpd (for music)

All installation is done via the debos scripts.
The desktop configuration is also done during the image creation but is housed at the following [git](https://gitlab.com/waldekg/mobian-i3wm-settings).
This give the possibility of updating just the settings and layout on a running phone.

You can build your own image and run it on the pinephone following the instructions below.
No actual phone utilities are included currently.
It has been put together to try and offer some [sxmo](https://sr.ht/~mil/Sxmo/) *like* functionality mobian.
From a user standpoint the idea is to configure the phone over ssh and have a basic UI to use it on the go.
The project is not at all on a fixed trajectory so all input is welcome.
Expect breaking changes.

For more general information you should visit [mobian-recipes](https://gitlab.com/mobian1/mobian-recipes) and [debos](https://github.com/go-debos/debos).

As in mobian-phosh the default user is `mobian` with password `1234`.

## Build

To build this image you'll need the following dependencies on a **bullseye** system.
I use a VM to build it on a thinkpad x220 and it takes about 40 minutes.

```
sudo apt install debos bmap-tools user-mode-linux
```

Clone this repository, move to the cloned `mobian-i3wm` folder and execute `./build.sh` as root.

```
sudo ./build.sh
```

### Modifying the build

The biggest changes compared to the mobian debos repo are found in the following files:

* user [packages](https://gitlab.com/waldekg/mobian-i3wm/-/blob/master/include/packages-i3wm.yaml)
* system [packages](https://gitlab.com/waldekg/mobian-i3wm/-/blob/master/devices/sunxi/packages-i3wm.yaml)
* the build [recipe](https://gitlab.com/waldekg/mobian-i3wm/-/blob/master/rootfs.yaml)
* an additional [script](https://gitlab.com/waldekg/mobian-i3wm/-/blob/master/scripts/setup-user-settings.sh)

# Usage tips

## Login

**You need to select the mobian user in lightdm to login**

See [this issue](https://gitlab.com/waldekg/mobian-i3wm/-/issues/3) for more information.

## Connecting

Connect your pinephone with USB to you computer and connect to it over ssh.
For me the pinephone was always on 10.66.0.1 but your milage may vary.

```
ssh mobian@10.66.0.1
```

Once you have a shell I would advise to set the DISPLAY evironment variable.
This way you can launch X application onto the screen of the pinephone itself.

```
export DISPLAY=:0
```

Now you should launch a tmux session so you can multitask like a pro.
Every window you create will have the DISPLAY variable set so you're good to go.

```
tmux
```

## Configuration

All configuration is done via config files coming from this [git](https://gitlab.com/waldekg/mobian-i3wm-settings).
This git is cloned during image creation to the following location:

```
cd ~/.mobian-i3wm-settings
```

The setup.sh script is called after cloning which places add files in their right location, plus some extra stuff.
It should be called as root with the $USERNAME set to mobian.


```
sudo ~/.mobian-i3wm-settings/./setup.sh mobian
```

It's probably not the best way to add a basic update system but for now it works.

### i3wm

i3wm is configured via a user configuration file that can be edited with the following command.

```
vim ~/.config/i3/config
```

### Onboard (virtual keyboard)

Onboard is configured via user configuration file that can be edited with the following command.

```
vim ~/.config/onboard/onboard-defaults.config
```

An additional numpad layout, used for pincode input, is created.
You can edit it with the following command.

```
sudo vim /usr/share/onboard/layouts/Numpad.onboard
```

### Polybar

The polybar layout and scripts are located at:

```
cd ~/.config/polybarpine
```

That folder is a shameless clone of [polybar-themes](https://github.com/adi1090x/polybar-themes/), but stripped down to only the [forest](https://github.com/adi1090x/polybar-themes/tree/master/simple/forest) theme which has been modified to work better on the pinephone.

### Rofi

Rofi is always called via polybar scripts.
The source directory for all rofi configuration can be found here:

```
cd ~/.config/polybarpine/forest/scripts/rofi
```

### URXVT
URXVT is configured via systemwide Xresources configuration file that can be edited with the following command.

```
sudo vim /etc/X11/Xresources/x11-i3wm
```

# License

This software is licensed under the terms of the GNU General Public License,
version 3.
