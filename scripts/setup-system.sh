#!/bin/sh

# Setup hostname
echo $1 > /etc/hostname

# Change plymouth default theme
plymouth-set-default-theme mobian

if [ -f /usr/sbin/lightdm ]; then
    systemctl enable lightdm.service
fi
