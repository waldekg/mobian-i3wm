#!/bin/sh

USERNAME=$1

# clone the settings repo and run the setup.sh script
su - $USERNAME -c "git clone https://gitlab.com/waldekg/mobian-i3wm-settings.git ~/.mobian-i3wm-settings && echo done" 
bash /home/$USERNAME/.mobian-i3wm-settings/./setup.sh $USERNAME




