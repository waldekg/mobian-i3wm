#!/bin/sh

USERNAME=$1
[ "$USERNAME" ] || exit 1
PASSWORD=$2
[ "$PASSWORD" ] || exit 1

adduser --gecos $USERNAME --disabled-password --shell /bin/bash $USERNAME
adduser $USERNAME sudo

# Needed for hardware access rights
adduser $USERNAME video
adduser $USERNAME render
adduser $USERNAME audio
adduser $USERNAME bluetooth
adduser $USERNAME plugdev
adduser $USERNAME netdev
adduser $USERNAME input
adduser $USERNAME dialout
adduser $USERNAME feedbackd

echo "$USERNAME:$PASSWORD" | chpasswd

mkdir -p /etc/lightdm/
mkdir -p /etc/onboard
su - mobian -c "mkdir -p ~/.config/i3"
su - mobian -c "mkdir -p ~/.config/onboard"
su - mobian -c "mkdir -p ~/.config/touchegg"
su - mobian -c "mkdir -p ~/.config/polybarpine"
su - mobian -c "mkdir -p ~/.local/bin/"
